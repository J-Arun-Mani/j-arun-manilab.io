---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
summary: "Summary of the project."
tags: 
  - a
  - b
repository: "https://gitlab.com/arun-mani-j/"
---

Details about the project.
