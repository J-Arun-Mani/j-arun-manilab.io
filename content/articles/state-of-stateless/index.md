---
title: "State of Stateless - DebConf 2023"
date: 2023-09-17T21:29:49+05:30
draft: false
summary: "My talk about immutability, reproducibility and declarative system in Debian at DebConf 2023."
tags: 
  - btrfs
  - debian
  - declarative
  - fedora
  - flatpak
  - guix
  - immutability
  - libostree
  - nix
  - reproducibility
---

I presented [this
talk](https://debconf23.debconf.org/talks/41-state-of-stateless-a-talk-about-immutability-and-reproducibility-in-debian/)
in DebConf 2023, Kochi, India.

- [Slides](state-of-stateless.pdf)
- [Video - 166
  MB](https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/debconf23-242-state-of-stateless-a-talk-about-immutability-and-reproducibility-in-debian.av1.webm)
- [Video - 81 MB](https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/debconf23-242-state-of-stateless-a-talk-about-immutability-and-reproducibility-in-debian.lq.webm)

Below is the gist.

Debian is one of the most used Linux operating systems. While RPM based OSes
like Fedora, OpenSUSE are moving to Transaction-Rollback style package
management, the scenario in Debian is still untouched.

In our talk, we explore and propose a user-perspective view on how a transaction
based system can be introduced in Debian. We discuss about various technologies
like layering of file system, taking advantage of BTRFS, and snapshot algorithms
etc.

We also shed light on the new trends in Linux ecosystem like Flatpak, Nix, Guix
and similar isolated package management systems.

Our talk is aimed at everyday users of Debian as the main motive of Debian is to
ensure stability, having a deterministic and reproducible packages ensures that
Debian systems can be deployed and used by users with least technical knowledge.
This also means that Debian can be used in enterprise institutes with a stable
base and fully-flexible changes to the packages in it. This can allow
reduced-downtime updates of applications and services.

In short we enhance Debian’s 007 principle. 0 stability issues, 0 security
issues and 7000 reasons to use Debian.

We highly believe that this talk will act as a stepping stone in bringing up the
features that shall enhance the traditional operating system metaphor.
