---
title: "New Website"
date: 2022-02-03T00:49:24+05:30
draft: false
summary: "A post about the making of this website."
tags:
  - daisyui
  - emacs
  - hugo
  - sway
  - tailwindcss
---

This is the first blog post I'm publishing in this website. I just finished adding final touch-ups to the site, which took me a three long evenings to make. With the finish, I thought of removing the placeholder posts (if you want to see those placeholders, then you can look at the source code repository 😉) and adding _real_ articles. Yes, this is a real article.

## Preface

To get started, I'm not a professional website developer, which you might have guessed with how the site looks 🙈, but still, I thought of giving it a try. I'm not really a fan of making websites because

- They are really difficult
- I'm not that creative
- I don't have enough resources

The above statements might sound vague to you, so let me explain.

> They are really difficult

Yes. Gone the days with simple websites. Nowadays every website has a pretty complexity around it. From a rich collection of frameworks to a massive list of variations like browsers (Chrom\*, Firefox, Safari etc.), screen sizes (desktop, phone, tablet etc.), personal preferences (dark mode, privacy conscious etc.). A professional website developer may not be worried of these, but as I said in the starting, I'm not a _professional_. So, it is not really easy from a newbie point of view.

> I'm not that creative

Creativity and me are opposite ends. I'm not really a fan of eyes-stealing fancy frills. I like to keep things simple, easy and tweak-able. Apart from that, I'm a sort of person who feels that I should not spend too much time in something that I don't understand quite well. An example is navigation menu. I wanted to avoid JavaScript at the _client_ side, so I looked for alternatives. One such, I found is a [hack using checkbox](https://css-tricks.com/the-checkbox-hack/#hide-the-sidebar). I tried it, it neither worked nor I understood why. I left it.

> I don't have enough resources

This is a big obstacle in my tech life and so it is for web development. I have a laptop with 2 GB RAM. Yep very old one. You know how difficult it is even to browse modern day websites, then how can I even dream of making one. Thanks to low-memory environment I have made myself comfortable of, I was able to make this site finely. The low-memory environment is nothing but [SwayWM](https://swaywm.org) and [GNU Emacs](https://www.gnu.org/software/emacs/). I will explain about it later.

Now you might get an idea on the problems I face. Yet, I was able to finish the site by doing _this and that_. Oh yes!

## My Old Website

The first real world website I made is a website for me. I was a very newbie to web development that time. I wanted a website for _namesake_. So I used (still use) [GitLab Pages](https://pages.gitlab.io) for hosting. I wrote the code using [VueJS](https://vuejs.org) and it was a very fine mess. But it did work.

It was not made for blog or ideas sharing, but just a website for namesake. The site was not colorful, it was full black text on a clear white background.

Soon after making the site, I forgot to improve it and later forgot its very existence.

## Bedtime Inspiration

Thanks to the COVID-19 pandemic, I got a lot of free time. In that free time, I was able to meet new people of different point of views (online and in Telegram of course!). From them, I learnt of new technologies and frameworks. But apart from those frameworks, I learnt about _simple awesome_ websites. There are a lot to name. Those are the websites that don't use JavaScript or try to nuke your privacy with ads and data harvest.

Examples include websites of [GNU](https://www.gnu.org), [Debian](https://www.debian.org), [Drew DeVault](https://drewdevault.com/). They were simply fascinating, and that's how I was also inspired to make a website that is simple. That's it.

## Time To Start Playing

So now it's time to start making the website. I decided to use [Hugo](https://gohugo.io) for building the website and [TailwindCSS](https://tailwindcss.com) for CSS. Now after the finish of site, I think I have made a solid decision to use them. They have good documentation and a pretty big community. It really helped.

Also thanks to a vast resources and tutorials available in Internet guiding various topics like semantic web, image optimization etc.

Finally, I solved the first two issues I mentioned [earlier](#preface), what about the third issue?

{{< figure src="workspace_one.webp" alt="An image showing my first workspace with Firefox open" caption="First workspace with Firefox open." >}}

I used to keep three workspaces in Sway. The first has Firefox with two tabs. The first tab has `localhost:1313` with my developing website. The second tab has documentation of either Hugo or TailwindCSS or any important site.

{{< figure src="workspace_two.webp" alt="An image showing my second workspace with Emacs open" caption="Second workspace with Emacs open." >}}

The second workspace has Emacs with code of my site. I don't know how many buffers will be active there, most of the time it is around ten. But at the front (means at Emacs window), I keep only one or two panes at most (two panes if I'm referencing some other code).

{{< figure src="workspace_three.webp" alt="An image showing my third workspace with Foot windows open" caption="Third workspace with Foot windows open." >}}

The third workspace has two [Foot](https://codeberg.org/cglogic/foot) terminal windows. Since the website is in development stage, I run watch mode of TailwindCSS and Hugo in the two windows. Watch mode is nothing but a watchdog that reloads and builds the website on file changes.

With this setup, my memory usage comes around 80%. And sometimes I need to debug the site, so I have to open Firefox's developer tools. So I'm pretty choking my system. But, this choking is the main reason, I need the website to be minimal. Neither can I make a website that is greedy on resources without killing my laptop 🤷.

## Conclusion

After pretty good headache, I finally finished the site. Also, for your kind information, I didn't jump directly from my old VueJS based site to this one. In between, I spent a lot of time in making 2 to 3 websites for my friends and contacts. For them, I used Hugo and TailwindCSS. This strengthened my experience and helped me in making my site without a lot of hassle.

My first blog post should not be so long, so let's finish it up. The website's source code is available for public reference in my [GitLab repository](https://gitlab.com/j-arun-mani/j-arun-mani.gitlab.io), feel free to have a look. Don't forget to share your feedback and thoughts via any Telegram or EMail (links are there in footer!).

Thanks for your time 😊.

## New Update

Time has changed, I bought a new laptop - ThinkPad E14. Now, the memory constraints are pretty much not an issue. I also learnt about [daisyUI](https://daisyui.com) and rewrote the website with a few parts using it. A lot of hard coded values were replaced with the help of daisyUI style classes. I'm now using [GNOME](https://www.gnome.org) but GNU (Doom) Emacs is still my default text editor. Somethings don't change right 😉?
