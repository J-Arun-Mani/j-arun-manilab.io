---
title: "Quack"
date: 2022-02-03T20:28:08+05:30
draft: false
summary: "A command-line application to search and get results via DuckDuckGo."
tags:
  - cli
  - duckduckgo
  - rust
  - web
repository: "https://gitlab.com/arun-mani-j/quack"
---

|            |                                      |
| ---------- | ------------------------------------ |
| Language   | Rust                                 |
| Repository | https://gitlab.com/arun-mani-j/quack |

Quack is a command-line app to search the web via DuckDuckGo. It uses DDG's API to fetch the instant answers and results.

The project is very simple in nature with the main objective to learn and try out Rust for something realistic. The app uses [`reqwest`](https://github.com/seanmonstar/reqwest) for the HTTP requests, [`clap`](https://github.com/clap-rs/clap) for command-line handling and [`serde_json`](https://github.com/serde-rs/json) to parse JSON.
