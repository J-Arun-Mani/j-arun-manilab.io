---
title: "aurthy"
date: 2023-01-02T00:50:16+05:30
draft: false
summary: "aurthy lets you sign and verify docs."
tags:
  - authentication
  - fastapi
  - nuxt
  - postgresql
  - python
  - typescript
  - verification
repository: "https://gitlab.com/aurcc/aurthy"
---

|            |                                                                    |
| ---------- | ------------------------------------------------------------------ |
| Languages  | Python, TypeScript                                                 |
| Frontend   | Nuxt, TailwindCSS, daisyUI                                         |
| Backend    | FastAPI                                                            |
| Database   | PostgreSQL                                                         |
| Repository | [https://gitlab.com/aurcc/aurthy](https://gitlab.com/aurcc/aurthy) |

aurthy is a [university](https://www.aurcc.in) project that lets a person or entity sign and distribute documents digitally. The receiving person can verify the originality by using the unique identifier. The aim of the project was to reduce the paperwork and go completely digital for everything from certificates to permission forms etc.

The working principle behind aurthy is simple. Each user gets a username and password. After logging in to their account, they can upload the documents and provide a username of the signer. The signer then receives the request, where they verify the credentials and approve the request. This generates a unique identifier using UUID and a certificate with QR code in it, which is then sent to the requester. Using the certificate, anybody can verify the authentication by scanning the QR code.

The threat model here is that the server is safe and if the server is able to verify a certificate then the certificate is valid.

The backend was written in [Python](https://www.python.org) using [FastAPI](https://fastapi.tiangolo.com/) framework. Frontend uses [Nuxt](https://nuxt.com) with [TailwindCSS](https://tailwindcss.com) and [daisyUI](https://daisyui.com) for CSS. The data regarding users and documents were stored in a [PostgreSQL](https://www.postgresql.org) database.

The project is currently in a halt because the HR team of the college said that many firms do not accept digital signatures. Plans are there to expand the scope of digital certificates using government schemes.
