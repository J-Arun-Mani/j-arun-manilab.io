---
title: "Jaya"
date: 2023-08-03T19:58:17+05:30
draft: false
summary: "If you want to read Mahabharta in short time, then this is the book."
tags:
  - classic
  - fantasy
  - hindu
  - mythology
author: "Devdutt Pattanaik"
link: "https://www.goodreads.com/book/show/19199728-jaya"
---

Since my childhood, I always wanted to read Mahabharta. I tried once in my
school days but didn't have a nice time thanks to the number of characters and
the family tree.

During my semester break, I thought I should try again but with a different
book. The main reason is that I wanted to read the philosophy.

However, most of the books were really huge and had the old English. Then I
found this book in Quora.

If you are beginner and like me — just wants the story without _beating around
the bush_, then this is book for you. There are no paragraphs of adjectives or
_simply talking_. The author comes to the point and has very crisp sentences.
Each of the chapter is very small and it manages to tell you the essence. Apart
from that, each chapter comes with a list of notes that talks about the
variations, contemporary nature etc.

Such a wonderful book in itself. Highly recommended.

Okay, my thoughts on Mahabharta itself? It is very cunning and smart way of
bending the rules, especially in the warfare. It is awesome. Though I feel like
it is injustice sometimes, the other voice in me says - _it is what it is,
everything got perfectly balanced_. So, once again, really awesome.
