---
title: "The Handmaid's Tale"
date: 2021-09-02T11:51:32+05:30
draft: false
summary: "If this is the future, then please don't."
tags:
  - fiction
  - fantasy
  - future
author: "Margaret Atwood"
link: "https://openlibrary.org/works/OL675783W/The_Handmaid%27s_Tale"
---

If this is the future, then please don't.
