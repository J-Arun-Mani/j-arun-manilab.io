---
title: "Purple Hibiscus"
date: 2024-11-10T14:07:10+05:30
draft: false
summary: "A rich family may not mean a happy life."
tags:
  - abuse
  - africa
  - childhood
  - fiction
  - nigeria
  - rich
author: "Chimamanda Ngozi Adichie"
link: "https://www.goodreads.com/book/show/14569052-purple-hibiscus"
---

This book was chosen by my friend [Frida](https://fable.co/frida-275611689534)
to _bring back_ my reading habit. She set a schedule of how much we both can
read daily and at the end of the day, we did a short review of how the story was
so far (also known as _buddy reading_). Thanks a lot Frida!

The story is different from other books I have read such that when I finished
it, it left me in an _ahem okay?_ state. The story is about a fifteen-year-old
girl from a wealthy household in Nigeria. She has a very powerful,
_dictator-like_, religious father. She describes a few events from her life like
her experience in her aunt's poor home, political issues in Nigeria etc.

The story and flow of reading was smooth. Tolerating a few unnecessarily
exhaustive review of trees, houses etc. the sentences were crisp and to the
point. It talked about a lot of social topics like politics, money, corruption
etc. I liked how they are told from the narrator's perspective.

To me, the plot itself felt incomplete. It felt like, instead of reading a full
story, you skip a few chapters before and after the climax. The main character
in the book is the narrator's father. Throughout the book, the narrator has a
_love-hate_ relationship towards him, making every move wondering _what would
father think if I do X?_. Given how important the father character is, the story
didn't fill all the missing pieces about him.

Then as we reach the _end-of-the-book_, we have to keep wondering what the
climax will be like because the plot is going in a different direction. And
suddenly you get the climax. The climax was super lame. But given how the story
was written in a _diaryish_ feel, I think the lame climax is acceptable as
that's how things happen in reality.

I'm not a fan of mixing languages. The story had a lot of short
[Igbo](https://en.wikipedia.org/wiki/Igbo_language) phrases. For a few, you
could understand their meaning from the context but for others it felt
distracting.

To summarize, the book was nice, the story was good enough. But it is just my
_perspective-based_ opinion. If you see the story as a short extraction of
chapters from the narrator's diary, then it is good. But if you, like me, wanted
the story to give more details regarding the father and his different _masks_ to
world, then you might be disappointed.
