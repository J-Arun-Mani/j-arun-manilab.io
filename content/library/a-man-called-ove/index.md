---
title: "A Man Called Ove"
date: 2022-01-07T13:41:41+05:30
draft: false
summary: "Ove is the chad neighbor we all hate but at the end miss."
tags:
  - fiction
  - friendship
  - old-age
author: "Fredrik Backman"
link: "https://openlibrary.org/works/OL16793865W/En_man_som_heter_Ove?edition=mancalledove0000back_n8o6"
---

Ove is the chad neighbor we all hate but at the end miss.
