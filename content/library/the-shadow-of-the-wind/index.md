---
title: "The Shadow of the Wind"
date: 2023-04-25T23:52:20+05:30
draft: false
summary: "A tospy-turvy ride through the knots of love."
tags:
  - conspiracy
  - fiction
  - love
  - revenge
  - war
author: "Carlos Ruiz Zafón"
link: "https://www.goodreads.com/book/show/1232.The_Shadow_of_the_Wind"
---

After a long time (may be not so long), I read a book with 400+ pages. I should
say that the efforts paid were completely worth. I had to read the book amidst a
busy schedule like project submissions and internal exams. So I spent an hour or
so whenever I felt like I need to forget the real world. Sometimes I even forced
myself to read the book because the story really pulled me in. 🙈

What's the speciality of the book? The protagonist (actually, the book doesn't
have a _single_ one) is a hero with no magical powers. He goes through a lot
of hard-times and disappointments. Almost every character you encounter in the
story has a very pitiful past in one way or the other. May be, this environment
gave the book a _realistic_ feel.

As a personal favorite, all chapters were with ten pages, which is satisfying if
you are someone who counts progress based on number of chapters you have read.
🙂

I don't want to spoil the story, but if you are a fan of thrillers, twists and
especially love, then this book can be a great time sucker. Bear in mind, the
love you are about to see also includes some heart breaks.

Finally, such a well written book and I enjoyed it. 😊
