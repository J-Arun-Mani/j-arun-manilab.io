---
title: "The Pregnant King"
date: 2022-05-02T14:09:51+05:30
draft: false
summary: "A wonderful combination of magic and philosophy."
tags:
  - fiction
  - mythology
  - fantasy
author: "Devdutt Pattanaik"
link: "https://www.goodreads.com/book/show/3086194-the-pregnant-king"
---

A wonderful combination of magic and philosophy.
