---
title: "Strange Planet"
date: 2021-07-02T11:51:09+05:30
draft: false
summary: "After reading, you realize that Earth is really strange."
tags:
  - fiction
  - parody
  - comics
author: "Nathan Pyle"
link: "https://openlibrary.org/books/OL28731520M/Strange_Planet"
---

After reading, you realize that Earth is really strange.
