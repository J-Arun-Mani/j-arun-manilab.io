---
title: "Anxious People"
date: 2021-08-02T11:51:19+05:30
draft: false
summary: "A refreshing tale of our human lives."
tags:
  - fiction
  - humor
author: "Fredrik Backman"
link: "https://openlibrary.org/works/OL20793817W/Anxious_People"
---

A refreshing tale of our human lives.
