---
title: "Sherlock Holmes"
date: 2021-03-02T14:09:27+05:30
draft: false
summary: "Each story teaches you a new form of out-of-box thinking."
tags:
  - fiction
  - mystery
  - detective
author: "Arthur Conan Doyle"
link: "https://www.goodreads.com/book/show/188572.The_Complete_Sherlock_Holmes"
---

Each story teaches you a new form of out-of-box thinking.
