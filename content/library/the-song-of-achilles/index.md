---
title: "The Song of Achilles"
date: 2021-06-02T11:50:58+05:30
draft: false
summary: "Trojan War but from a different amazing viewpoint."
tags:
  - mythology
  - trojan
  - war
author: "Madeline Miller"
link: "https://openlibrary.org/works/OL16509148W/The_Song_of_Achilles"
---

Trojan War but from a different amazing viewpoint.
