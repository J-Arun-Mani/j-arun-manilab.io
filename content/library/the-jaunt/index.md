---
title: "The Jaunt"
date: 2021-11-19T13:41:19+05:30
draft: false
summary: "A future that is scary because of science."
tags:
  - fiction
  - scifi
  - horror
author: "Stephen King"
link: "https://www.goodreads.com/book/show/52317884-the-jaunt"
---

A future that is scary because of science.
