---
title: "Nothing Lasts Forever"
date: 2020-01-20T13:42:18+05:30
draft: false
summary: "Mysteries within mysteries."
tags:
  - fiction
  - medical
  - mystery
  - murder
author: "Sidney Sheldon"
link: "https://openlibrary.org/works/OL19349115W/Nothing_Lasts_Forever?edition=key%3A/books/OL26806052M"
---

Mysteries within mysteries.
