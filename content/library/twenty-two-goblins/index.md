---
title: "Twenty Two Goblins"
date: 2024-01-11T19:45:33+05:30
draft: false
summary: "Classic Indian folklore told in a simplistic manner."
tags: 
  - classic
  - fiction
  - mythology
  - vampire
author: "Arthur W. Ryder"
link: "https://www.goodreads.com/book/show/1494017.Twenty_Two_Goblins"
---

When I was young, a friend of mine told me a story of three skilled men and
asked me who is the _best_ among them. I forgot what answer I gave. But I
started liking such stories where the conclusion is left to the reader.

[Vikram-Vetala](https://en.wikipedia.org/wiki/Vetala_Panchavimshati) is a story
about a king who is tasked by a sorcerer to bring a vampire to him. The vampire
tells the king a few stories. At the end of each story, the vampire asks the
king a question. If the king knows the answer but doesn't reply, his head will
burst into pieces. If he answers, the vampire will escape back to its origin. So
for every story, the vampire flies back and the king goes back to fetch it. This
continues for about twenty two stories (or twenty five as per Wikipedia).

After reading Mahabharata and a few other stories of Indian mythology, I thought
of reading Vikram-Vetala tales too. I used Wikipedia as a reference to suggest
me books. First I tried [Richard Burton's
retelling](https://archive.org/details/cu31924024159760) but it was verbose
and its language not easy for me.

Then I tried Twenty Two Goblins. The book suited me well because it was short
and the language felt like literal translation. Like, the author even translated
the names of people and cities to English. 

Most of the stories had a similar plot. A love affair and almost all of them a
_love at the first site_. The answers by king were also just and reflective,
though a few controversial as per modern trends. Anyway, the stories flow
through the classical Indian rules of justice, hierarchy and authority, which is
good enough to keep you _day dreaming_.

Both of these books are in public domain, so you can read them right away 😇.
1. [Twenty-Two Goblins in Project
   Gutenberg](https://www.gutenberg.org/ebooks/2290)
2. [Vikram and the Vampire in Project Gutenberg](https://www.gutenberg.org/ebooks/2400)   
